package com.niit.myorm.config;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.niit.myorm.model.Employee;

@Configuration
@EnableTransactionManagement

public class AppConfig {
	
	@Bean
	public DataSource getDatasource()
	{
		BasicDataSource datasource=new BasicDataSource();
		datasource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		datasource.setUrl("jdbc:mysql://localhost:3306/CTSfeb");
		datasource.setUsername("root");
		datasource.setPassword("password");
		return datasource;
	}
	
	@Bean
	
	public LocalSessionFactoryBean getsession(DataSource ds) throws IOException
	{
		LocalSessionFactoryBean sessionFactory=new LocalSessionFactoryBean();
		sessionFactory.setDataSource(ds);
		Properties property=new Properties();
		property.put("hibernate.show_sql","true");
		property.put("hibernate.dialect","org.hibernate.dialect.MySQL5Dialect");
		property.put("hibernate.hbm2ddl.auto", "update");
	sessionFactory.setHibernateProperties(property);
	sessionFactory.setAnnotatedClasses(Employee.class);
	sessionFactory.afterPropertiesSet();
		return sessionFactory;
	}
	
	@Bean
	
	public HibernateTransactionManager getmanager(SessionFactory sessfact)
	{
		HibernateTransactionManager hibernatemanager=new HibernateTransactionManager();
		hibernatemanager.setSessionFactory(sessfact);
		return hibernatemanager;
		
	}

}
