package com.niit.myorm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(basePackages="com.niit.myorm")
@EnableWebMvc

public class WebMVCconfig {

@Bean
public InternalResourceViewResolver viewResolve()
{
	InternalResourceViewResolver viewres=new InternalResourceViewResolver();
	viewres.setPrefix("/WEB-INF/views/");
	viewres.setSuffix(".jsp");
	return viewres;
	
}
	
}
