package com.niit.myorm.repository;

import java.util.List;

import com.niit.myorm.model.Employee;

public interface EmpDAO 
{
void saveRec(Employee eobj);
List<Employee> viewall();
boolean delrec(int id);
Employee viewbyId(int id);

}
