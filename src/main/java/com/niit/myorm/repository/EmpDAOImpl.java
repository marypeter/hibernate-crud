package com.niit.myorm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.niit.myorm.model.Employee;

@Repository
@Transactional

public class EmpDAOImpl implements EmpDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	public EmpDAOImpl(SessionFactory sess)
	{
		this.sessionFactory=sess;
	}
	
	public void saveRec(Employee eobj) {
	
		sessionFactory.getCurrentSession().save(eobj);
	}

	public List<Employee> viewall() {
		
		return sessionFactory.getCurrentSession().createQuery("from Employee").list();
	}

	public boolean delrec(int id) {
		
		
		Employee eobj=sessionFactory.getCurrentSession().load(Employee.class, id);
		sessionFactory.getCurrentSession().delete(eobj);
		// TODO Auto-generated method stub
		return true;
	}

	public Employee viewbyId(int id) {
Employee e=(Employee)sessionFactory.getCurrentSession().createQuery("from Employee where employeeid="+id).uniqueResult();
		return e;
	}

}
