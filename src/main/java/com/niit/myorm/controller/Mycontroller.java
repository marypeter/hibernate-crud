package com.niit.myorm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.niit.myorm.model.Employee;
import com.niit.myorm.repository.EmpDAO;

@Controller
public class Mycontroller {
	
	
	@Autowired
	EmpDAO empDao;
	
	
	
	@GetMapping ("/")
	public String first()
	{
		return "index";
	}
	
	@PostMapping("/store")
	
	public String addREc(@ModelAttribute("employee") Employee eobj,ModelMap mp)
	{
	empDao.saveRec(eobj);	
	mp.addAttribute("emplist",empDao.viewall());
	return "index";
	}
	
	@GetMapping("/deleterec")
	
	public String delrec(@RequestParam("empid") int eid,ModelMap mp)
	{
		empDao.delrec(eid);
		mp.addAttribute("emplist",empDao.viewall());
		return "index";
		
	}
	
//	public String viewrec()
	//{

}
